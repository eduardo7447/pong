package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Pelota {
	
	private double x;
	private double y;
	private double direccion;
	private double velocidad;
	private double angulo;
	private Image img ;
	private double tamaño;
	private Color color;
	
	public Pelota(double x, double y, double velocidad) {
		this.x = x;
		this.y = y;
		this.velocidad = velocidad;
		this.angulo = -Math.PI /4;
		this.tamaño = 50;
		this.color = Color.BLUE;
		this.img = Herramientas.cargarImagen("ball.png") ;
		this.direccion = this.angulo;
	}
	

	public double getX() {
		return x;
	}
	public double direccion() {
		return direccion;
	}
	public double angulo() {
		return angulo;
	}
	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getTamaño() {
		return tamaño;
	}

	public void setTamaño(double tamaño) {
		this.tamaño = tamaño;
	}

	public void dibujar(Entorno e) {

		e.dibujarImagen(img, x, y, 0, 0.3);
		e.dibujarCirculo(x, y, tamaño, color);
	}

	public void mover() {
		x += velocidad * Math.cos(angulo );
		y += velocidad * Math.sin(angulo);
	}
//|| y > entorno.alto()
	public boolean chocasteConElEntorno(Entorno entorno) {
		return y <tamaño / 2 || x > entorno.ancho()- tamaño/2 || x < tamaño/2 ;
	}

	public void cambiarDeDireccion() {
			angulo += Math.PI/4 ;
		
	}

	public void acelerar() {
		velocidad += 0.5;
	}

	// TODO
	public boolean chocasteCon(Slider slider) {
		return x < slider.getX()+slider.getAncho()/2
				&& x > slider.getX()-slider.getAncho()/2  
				&& y + tamaño/2+5> slider.getY() - slider.getAlto() /2 ;
	}

	public boolean pasasteDeLargo(Slider slider,Entorno e) {
		// TODO Auto-generated method stub
		return !chocasteCon(slider) && y > e.alto();
	}

	public void iniciaEnElCentro(Entorno entorno) {
		// TODO Auto-generated method stub
		x = entorno.ancho()/2 ;
		y = entorno.alto()/2 ;
	}

	public boolean regresasDeChoqueConSlider(Slider slider, Entorno entorno) {
		// TODO Auto-generated method stub
		return y + tamaño/2 -5> slider.getY() - slider.getAlto() /2 && !(y > entorno.alto());
	}


	public boolean chocasteConLadoDerecho() {
		return x >tamaño/2 -5;
	}


	public boolean chocasteConLadoIzquierdo() {
				return tamaño /2 < x+5;
	}

	public boolean chocasteConLadoSuperior() {
		// TODO Auto-generated method stub
		return false;
	}


	public boolean colisionIzquierda(Entorno entorno) {
		// TODO Auto-generated method stub
		return tamaño >x;
	}


	public boolean estasPorChocarConSlider(Slider slider, Entorno entorno) {
		// TODO Auto-generated method stub
		return y > entorno.alto() - tamaño && x < slider.getX()+slider.getAncho()/2
				&& x > slider.getX()-slider.getAncho()/2 ;
	}
	

	
}

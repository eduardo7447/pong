package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Image fondo ;
	private Pelota pelota;
	private Slider slider;
	private int puntaje ;
	private double x, y , aux3;
	
	public Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "rebota pelota rebota", 800, 600);
		
		// Inicializar lo que haga falta para el juego
		// ...

		pelota = new Pelota(400, 300, 3);
		slider = new Slider(entorno.ancho()/2,entorno.alto()-10,5);
		this.fondo = Herramientas.cargarImagen("pasto.png") ;
		this.aux3 = 0 ;
		this.puntaje = 0 ;
		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick() {
		entorno.dibujarImagen(fondo, entorno.ancho()/5, entorno.alto()/2, 0);
		
		pelota.dibujar(entorno);
		slider.dibujar(entorno);
		
		entorno.cambiarFont("sans", 35, Color.RED);
		entorno.escribirTexto("puntaje: " + puntaje, entorno.ancho() / 2 - 350, 30);
		entorno.escribirTexto("direccion: " + pelota.direccion(), entorno.ancho() / 2 - 350, 60);
		entorno.escribirTexto("x: " + x, entorno.ancho() / 2 - 350, entorno.alto() - 35);
		entorno.escribirTexto("y: " + y, entorno.ancho() / 2 - 350, entorno.alto() - 5);

		pelota.mover();
//		if (pelota.colisionIzquierda(entorno)) {
//			puntaje++;
//		}
		if (pelota.chocasteConElEntorno(entorno)) {
			x = pelota.getX();
			y = pelota.getY();
			System.out.println(puntaje);
			pelota.cambiarDeDireccion();
		}

		if (entorno.estaPresionada('s')) {
			pelota.acelerar();
			slider.acelerar();

		}
		
		if (entorno.estaPresionada('a')) {
			slider.moverHaciaIzquierda();
		}
		
		if (entorno.estaPresionada('d')) {
			slider.moverHaciaDerecha();
		}
		
		if(slider.chocasteConLaIzquierdaDelEntorno()){
			slider.reiniciarPosicionX();
		}
		if(slider.chocasteConLaDerechaDelEntorno(entorno)){
			slider.posicionDerechaX(entorno);
		}
		if(pelota.chocasteCon(slider)) {
			pelota.cambiarDeDireccion() ;
			
		}
		if(pelota.pasasteDeLargo(slider, entorno)) {
			pelota.iniciaEnElCentro(entorno);
			pelota.cambiarDeDireccion();
		}

	if(pelota.estasPorChocarConSlider(slider,entorno)) {
		puntaje++;
	}
	

//pelota.posicion().mostrar();
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}

package juego;

import java.awt.Color;

import entorno.Entorno;

public class Slider {

	private double x;
	private double y;
	
	private double velocidad;
	
	private double ancho;
	private double alto;

	private Color color;
	public Slider(double x, double y, double velocidad) {
		this.x = x;
		this.y = y;
		this.velocidad = velocidad;
		this.ancho = 200;
		this.alto = 20;
		this.color = Color.YELLOW;
	}

	public void dibujar(Entorno e) {
		e.dibujarRectangulo(x, y, ancho, alto, 0, color);
	}

	public void moverHaciaIzquierda() {
		x -= velocidad;
	}

	public void moverHaciaDerecha() {
		x += velocidad;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}

	public void acelerar() {
		velocidad += 1;
		
	}


	public void cambiaDireccion() {
		velocidad*=-1 ;
	}

	public boolean chocasteConLaIzquierdaDelEntorno() {
		// TODO Auto-generated method stub
		return x < ancho/2;
	}

	public void reiniciarPosicionX() {
		x = ancho/2 ;
	}

	public boolean chocasteConLaDerechaDelEntorno(Entorno e) {
		// TODO Auto-generated method stub
		return x > e.ancho()- ancho/2;
	}

	public void posicionDerechaX(Entorno entorno) {
		// TODO Auto-generated method stub
		x = entorno.ancho()-ancho/2;

	}


	
	
	
}
